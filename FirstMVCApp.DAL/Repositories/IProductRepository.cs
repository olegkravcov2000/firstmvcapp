﻿using System.Collections.Generic;
using FirstMVCApp.DAL.Entities;

namespace ConsoleAppWithDb.Repositories
{
    public interface IProductRepository : IRepository<Product>
    {
        Product GetTheMostExpensiveProduct();
        IEnumerable<Product> GetAllWithCategoriesAndBrandsByPrice(decimal priceFrom, decimal priceTo);
        IEnumerable<Product> GetAllWithCategoriesAndBrands();
    }
}
