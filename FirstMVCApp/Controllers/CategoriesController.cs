﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstMVCApp.DAL;
using FirstMVCApp.Models;
using FirstMVCApp.Services;
using Microsoft.AspNetCore.Mvc;

namespace FirstMVCApp.Controllers
{
    public class CategoriesController : Controller
    {
        private readonly ICategoryService _categoryService;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        public CategoriesController(ICategoryService categoryService, IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (categoryService == null)
                throw new ArgumentNullException(nameof(categoryService));

            _categoryService = categoryService;
            _unitOfWorkFactory = unitOfWorkFactory;
        }


        public IActionResult Index()
        {
            using (var uow = _unitOfWorkFactory.Create())
            {
                var categories = uow.Categories.GetAll().ToList();
                return View(categories);
            }
        }
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(CategoryCreateModel model)
        {
            try
            {
                _categoryService.CreateCategory(model);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

        }
    }
}