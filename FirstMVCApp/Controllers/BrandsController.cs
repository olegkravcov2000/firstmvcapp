﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.Models;
using FirstMVCApp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Update;

namespace FirstMVCApp.Controllers
{
    public class BrandsController : Controller
    {
        private readonly IBrandService _brandService;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        public BrandsController(IBrandService brandService, IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (brandService == null)
                throw new ArgumentNullException(nameof(brandService));

            _brandService = brandService;
            _unitOfWorkFactory = unitOfWorkFactory;
        }
       


        public IActionResult Index()
        {
            using (var uow = _unitOfWorkFactory.Create())
            {
                var brands = uow.Brands.GetAll().ToList();
                return View(brands);
            }
        }

        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(BrandCreateModel model)
        {
            try
            {
                _brandService.CreateBrand(model);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
            
        }
    }
}