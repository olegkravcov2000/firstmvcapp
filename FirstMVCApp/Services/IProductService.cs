﻿using System.Collections.Generic;
using FirstMVCApp.Models;

namespace FirstMVCApp.Services
{
    public interface IProductService
    {
        List<ProductModel> SearchProducts(ProductFilterModel model);
        ProductCreateModel GetProductCreateModel();
        void CreateProduct(ProductCreateModel model);
    }
}