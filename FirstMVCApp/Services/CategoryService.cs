﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FirstMVCApp.DAL;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.Models;

namespace FirstMVCApp.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;


        public CategoryService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }
       

        public void CreateCategory(CategoryCreateModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var category = Mapper.Map<Category>(model);

                unitOfWork.Categories.Create(category);
            }
        }
    }
}
