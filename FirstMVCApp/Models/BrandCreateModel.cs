﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
namespace FirstMVCApp.Models
{
    public class BrandCreateModel
    {
        [Display(Name = "Наименование")]
        [Required(ErrorMessage = "Наименование должно быть заполненным")]
        public string Name { get; set; }
    }
}
