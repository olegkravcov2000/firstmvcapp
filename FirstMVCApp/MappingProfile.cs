﻿using AutoMapper;
using FirstMVCApp.DAL.Entities;
using FirstMVCApp.Models;

namespace FirstMVCApp
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateProductToProductModelMap();
            CreateProductCreateModelToProduct();
            CreateBrandCreateModelToBrand();
            CreateCategoryreateModelToCategory();
        }

        private void CreateProductToProductModelMap()
        {
            CreateMap<Product, ProductModel>()
                .ForMember(target => target.BrandName,
                    src => src.MapFrom(p => p.BrandId == null ? ProductModel.NoBrand : p.Brand.Name))
                .ForMember(target => target.CategoryName,
                    src => src.MapFrom(p => p.Category.Name));
        }

        private void CreateProductCreateModelToProduct()
        {
            CreateMap<ProductCreateModel, Product>();
        }
        private void CreateBrandCreateModelToBrand()
        {
            CreateMap<BrandCreateModel, Brand>();
        }
        private void CreateCategoryreateModelToCategory()
        {
            CreateMap<CategoryCreateModel, Category>();
        }
    }
}
